import scrapy

class StackOverflowSpider(scrapy.Spider):
	name = 'stackoverflow' # cada spider tiene un nombre único
	start_urls = ['http://stackoverflow.com/questions?sort=votes']
	specific set of urls
	# el análisis comienza desde un 
	def parse(self, response): # para cada solicitud que genera este generador, su respuesta se envía a
		parse_question
		for href in response.css('.question-summary h3 a::attr(href)'): # realice algunas tareas de raspado
			using css selectors to find question urls
			full_url = response.urljoin(href.extract())
			yield scrapy.Request(full_url, callback=self.parse_question)
	def parse_question(self, response):
		yield {
			'title': response.css('h1 a::text').extract_first(),
			'votes': response.css('.question .vote-count-post::text').extract_first(),
			'body': response.css('.question .post-text').extract_first(),
			'tags': response.css('.question .post-tag::text').extract(),
			'link': response.url,
			}